﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;
using System.Diagnostics;

public class PlayerController : MonoBehaviour
{
    public float speed = 0;
    public TextMeshProUGUI countText;
    public GameObject winTextObject;

    private Rigidbody rb;
    private int count;
    private float movementX;
    private float movementY;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;

        SetCountText();
        winTextObject.SetActive(false);
    }

    void OnMove(InputValue movementValue)
    {
        Vector2 movementVector = movementValue.Get<Vector2>();

        movementX = movementVector.x;
        movementY = movementVector.y;
    }

    void SetCountText()
    {
        countText.text = "Count: " + count.ToString();
        if (count >= 51) //Score to win is now 51
        {
            winTextObject.SetActive(true);
        }
    }

    void FixedUpdate()
    {
        Vector3 movement = new Vector3(movementX, 0.0f, movementY);

        rb.AddForce(movement * speed);
    }

    //void OnTriggerEnter(Collider other)
    //{
    //if (other.gameObject.CompareTag("PickUp"))
    // {
    //other.gameObject.SetActive(false);
    //count = count + 1;

    //SetCountText();
    // }
    // }

    //Based on the Color/Tag of the pickup they now give the player different amounts of points
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("PickUp"))
        {
            collision.gameObject.SetActive(false);

            count = count + 1;

            SetCountText();
        }
        
        if (collision.gameObject.CompareTag("PickUpGreen"))
        {
            collision.gameObject.SetActive(false);

            count = count + 5;

            SetCountText();
        }

        if (collision.gameObject.CompareTag("PickUpPurple"))
        {
            collision.gameObject.SetActive(false);

            count = count + 10;

            SetCountText();
        }
    }
}