﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunFromPlayer : MonoBehaviour
{

    public GameObject player;
    Rigidbody rb;

    public float speed;
   
    private void Start()
    {
        rb = GetComponent<Rigidbody>(); 
    }

    void FixedUpdate()
    {
        if (Vector3.Distance(transform.position, player.transform.position) < 2f)
        {
            Vector3 dirAwayFromPlayer = transform.position - player.transform.position;
            dirAwayFromPlayer = dirAwayFromPlayer.normalized;

            rb.velocity += dirAwayFromPlayer * 1;
        }
        //If the player comes within 1units, they Pickup will slow down and be attracted to the player. Makes the game a little easier.
        if (Vector3.Distance(transform.position, player.transform.position) < 1f)
        {
            Vector3 dirAwayFromPlayer = transform.position - player.transform.position;
            dirAwayFromPlayer = dirAwayFromPlayer.normalized;

            rb.velocity += dirAwayFromPlayer * -1;
        }
        //If y position is more than 1, resets pickups y position to 1
        if (transform.position.y > 1)
        {
            transform.position = new Vector3(transform.position.x, 1, transform.position.z);
        }


    }
}
